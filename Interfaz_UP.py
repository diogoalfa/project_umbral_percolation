__author__ = 'macbookpro'
__author__ = 'carlos'
import sys
from PyQt4 import QtGui
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
import numpy as np
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QTAgg as NavigationToolbar
import matplotlib.pyplot as plt

#se crea una clase ventana que contendra la ventana y sus acciones
class ventana(QtGui.QWidget):

    #se declara la clase ventana
    def __init__(self):
        super(ventana, self).__init__()
        self.initUI()

    #se inicia la clase ventana
    def initUI(self):

        #se inician desde el titulo hasta la fuente a utilizar
        QtGui.QToolTip.setFont(QtGui.QFont('SansSerif',10))
        self.setToolTip(u'<b>PhisTechnical analysis </b>')
        self.setWindowTitle(u'PhisTechnical analysis ')
        self.setGeometry(500, 500, 600, 500)

        #declaracion del primer layoud horizontal
        self.LHorizontalSup = QtGui.QHBoxLayout()
        self.ElementosIzqSuperior()

        #declaracion del segundo layoud horizontal
        self.LHorizontalBot = QtGui.QHBoxLayout()
        self.elementosinferior()

        #declaracion del layoud vertical izquierdo superior que
        # contendra al los layoud horizontales
        self.LIVertical = QtGui.QVBoxLayout()
        self.LIVertical.addLayout(self.LHorizontalSup)
        self.LIVertical.addLayout(self.LHorizontalBot)

        #se declara el layoud vertical derecho superior
        # que contendra al grafico y al boton de exportar
        self.LDVertical = QtGui.QVBoxLayout()
        self.figure = plt.figure()
        self.canvas = FigureCanvas(self.figure)
        self.izquierdasuperior()

        #se declara una grilla que contiene a todos los
        # elementos declarados en la ventana
        grilla = QtGui.QGridLayout()
        grilla.addLayout(self.LIVertical,0,0)
        grilla.addLayout(self.LDVertical,0,1)
        self.setLayout(grilla)

    #se cargan los elementos perteneciente a la parte superior del sector superior
    # del layoud vertical (es el 1º layoud horizontal que carga)
    def ElementosIzqSuperior(self):

        self.lbl =QtGui.QLabel()
        lbl2 = QtGui.QLabel(u' matriz')
        texto =QtGui.QLineEdit()
        #el verifica el texto escrito para que aparesca en el label de la derecha
        texto.textChanged[str].connect(self.onChanged)
        lblX = QtGui.QLabel(u' x ')
        #se agregan los widget en orden al layoud horizontal
        self.LHorizontalSup.addWidget(lbl2)
        self.LHorizontalSup.addWidget(texto)
        self.LHorizontalSup.addWidget(lblX)
        self.LHorizontalSup.addWidget(self.lbl)

    #se cargan los elementos perteneciente a la parte baja del sector superior
    # del layoud vertical (es el 2º layoud horizontal que carga)
    def elementosinferior(self):

        #coneccion del boton con la funcion paralela
        self.btnP = QtGui.QPushButton(u'ejecucion paralela')
        self.btnP.clicked.connect(self.plotparalelo)
        #coneccion del boton con la funcion serial
        self.btnS = QtGui.QPushButton(u'ejecucion serial')
        self.btnS.clicked.connect(self.plotserial)
        #se agregan los widget
        self.LHorizontalBot.addWidget(self.btnP)
        self.LHorizontalBot.addWidget(self.btnS)


    #se cargan los elementos pertenecientes al
    # sector izquierdo superior de la ventana en un layoud vertical
    # (dentro del layoud vertica existen 2 layoud horizontales)
    def izquierdasuperior(self):

        btnE = QtGui.QPushButton(u'Exportar')
        self.LDVertical.addWidget(self.canvas)
        self.LDVertical.addWidget(btnE)

    #cuando cambia el dato del Qlinedata, el qlabel de la derecha
    #al lado de la x tambien cambia para que esta siempre sea
    # una matriz cuadrada
    def onChanged(self, text):

        self.lbl.setText(text)
        self.lbl.adjustSize()

    #muestra en el grafico canvas de la derecha el resultado del algoritmo serial
    def plotserial(self):

        X = np.random.rand(100)
        Y = X + np.random.rand(100)*0.1
        print(Y)
        print(X)
        ax = self.figure.add_subplot(111)
        ax.hold(False)
        ax.plot(X,Y, '.-')
        self.canvas.draw()

    #muestra en el grafico canvas de la derecha el resultado del algoritmo paralelo
    def plotparalelo(self):

        X = np.random.rand(100)
        Y = X + np.random.rand(100)*0.1
        ax = self.figure.add_subplot(111)
        ax.hold(False)
        ax.plot(X,Y, '.-')
        self.canvas.draw()


#se define el main para iniciar la aplicacion QtGui
def main():
    app = QtGui.QApplication(sys.argv)
    v = ventana()
    v.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()

