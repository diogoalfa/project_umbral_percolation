import math

__author__ = 'PhisTechnical Analysis'

import random
from StringIO import StringIO
import matplotlib.pyplot as plt
import numpy as np
#import sympy as sy
import time



RANGOMATRIZ = 20
CANTIDADINTENT=100   #cantidad de simulacion por llenado
#CANTIDADPUNTOS = RANGOMATRIZ**2 # cantidad de puntos en el grafico
CANTIDADPUNTOS = 100 # cantidad de puntos en el grafico

print "Rango Matriz: "+str(RANGOMATRIZ)
print "Cantidad de Puntos: "+str(CANTIDADINTENT)
print "Cantidad Puntos Grafico: "+str(CANTIDADPUNTOS)

class ManejoFichero:
    def __init__(self, nombreArchivo, accion):
        self.nombreArchivo = nombreArchivo
        self.accion = accion


    def grabarMatriz(self, matriz):
        if (self.accion == "w"):
            self.archivo = open(self.nombreArchivo, self.accion)
            for i in range(len(matriz)):
                for j in range(len(matriz[0])):
                    self.archivo.write(str(int(matriz[i][j])))
                    self.archivo.write(" ")
                self.archivo.write("\n")
            self.archivo.close()
            return "1"
        return 0

    def leerFichero(self):
        if (self.accion == "r"):
            self.archivo = open(self.nombreArchivo, self.accion)
            matriz = self.archivo.read()
            self.archivo.close()
            matriz = np.genfromtxt(StringIO(matriz))
            return matriz
        return 0

    def guardarDato(self, valorX, valorY):
        if (self.accion == "a"):
            self.archivo = open(self.nombreArchivo, self.accion)
            self.archivo.write(str(valorX) + " " + str(valorY) + "\n")
            self.archivo.close()

    def guardarDatoUnico(self, valorX):
        if (self.accion == "a"):
            self.archivo = open(self.nombreArchivo, self.accion)
            self.archivo.write(str(valorX)+"\n")
            self.archivo.close()

    def setAccion(self,accion):
        self.accion=accion
    def setNombreArchivo(self,nombreArchivo):
        self.nombreArchivo=nombreArchivo


# QuickFind = es una estructura de datos para guardar que numeros estan conectados y despues corroborar si esta conectados en tiempo nlog(n)

class QuickFind:
    def __init__(self, n):
        global id
        id = np.arange(0, n, 1)
        #print "rango id : " + str(len(id))

    def connected(self, id1, id2):
        if (id[id1] == id[id2]):
            return True
        else:
            return False

    def union(self, id1, id2):
        id_1 = id[id1]
        id_2 = id[id2]
        for i in range(len(id)):
            if (id[i] == id_1):
                id[i] = id_2


def llenarMatrizProbabilidad(rangoMatriz, probabilidad):
    matriz = np.zeros((rangoMatriz, rangoMatriz))
    cantPuntos = rangoMatriz * rangoMatriz
    #print "probabilidad de llenado +: " + str(probabilidad)
    cantLlenarPuntos = int(probabilidad * cantPuntos)
    #print "cantidad puntos por LLenar :" + str(cantLlenarPuntos)
    i = 1
    while (i <= cantLlenarPuntos):
        aleatorioX = random.randrange(rangoMatriz)
        aleatorioY = random.randrange(rangoMatriz)
        if (matriz[aleatorioX][aleatorioY] == 0):
            matriz[aleatorioX][aleatorioY] = i
            i += 1
    return matriz

def unirConexion(rangoMatriz,matrix,qFind):
    for j in range(0,rangoMatriz):
        for i in range(rangoMatriz-1):
            if(matrix[i][j]!=0 and matrix[i+1][j]!= 0):
                #print "matriz :"+str(matrix[i][j])+"| matriz :"+str(matrix[i+1][j])
                qFind.union(int(matrix[i][j]),int(matrix[i+1][j]))
            if(matrix[j][i]!=0 and matrix[j][i+1]!=0):
                qFind.union(int(matrix[j][i]),int(matrix[j][i+1]))
                #print (int(matrix[j][i]),int(matrix[j][i+1]))
        #print "Iter V: "+str(j)

def encontroPercolacion(dato1,dato2,q_find):
        if(q_find.connected(dato1,dato2)==True):
            return 1
        else:
            return 0



def umbral_de_percolacion(data):
    for i, y  in enumerate(data[:,1]):
        if(y > 0.5):
            umbral = (data[i,0]+data[i-1,0])/2
            return umbral

def intervaloConfianza(promUP,varianzaUP,largoMuestra):
    interX=promUP-(1.96*math.sqrt(varianzaUP)/math.sqrt(largoMuestra))
    interY=promUP+(1.96*math.sqrt(varianzaUP)/math.sqrt(largoMuestra))
    return interX,interY

def promPinflex(umbPer):
    mf=ManejoFichero("datosPercolacion.txt","a")
    mf.guardarDatoUnico(umbPer)
    mf.setAccion("r")
    datos=mf.leerFichero()
    #print "largo array datos : "+str(len(datos))+" | largo "+str(len(datos[0]))
    largo=len(datos)
    suma1=0
    for i in range(largo):
       if(datos[i]<0.9 and datos[i]<0.9):
           suma1=suma1+datos[i]
    promUP=suma1/largo
    suma3=0
    for i in range(largo):
        suma3=suma3+(datos[i]-promUP)**2
    varianza=suma3/largo
    return promUP,varianza,largo

def graficoDispersion(llenado,probabilidad):
    # X = llenado     Y= probabilidad
    plt.title("Probabilidad Percolacion vs Apertura")
    plt.plot(llenado,probabilidad)
    linTend =np.polyfit(llenado,probabilidad,5)
    polinomio =np.poly1d(linTend)

    #plt.plot(llenado,polinomio(llenado),"r--")
    # func=polinomio
    # deriv1=func.deriv()
    # deriv2=deriv1.deriv()
    # x=sy.symbols('x')
    # #--Elevado a 5 ---
    # d=deriv2[0]
    # c=deriv2[1]
    # b=deriv2[2]
    # a=deriv2[3]
    # sols=sy.solve(a*x**3 + b*x**2 + c*x + d,x)
    # y=sy.symbols('y')
    # n=sy.symbols('n')
    # h=sy.symbols('h')
    # pY=str(sols[1])
    # pI=""+pY[0]
    # pI=pI+pY[1]
    # pI=pI+pY[2]
    # pI=pI+pY[3]
    # pI=pI+pY[4]
    # piX=float(pI)
    # print "punto inflexion pol : "+str((piX,func(piX)))
    # plt.plot(piX,func(piX),"o")
    #-------------------------------
    data=np.array([llenado,probabilidad])
    data=data.T
    uP=umbral_de_percolacion(data)
    plt.plot(uP,0.5,"o")
    print "----------------------------------------"
    print "Umbral Percolacion :"+str(uP)
    promUP,varianzaUP,largoMuestra=promPinflex(uP)
    print "Promedio UP : "+str(promUP)
    print "Varianza UP : "+str(varianzaUP)
    print "intervalos de confianza : "
    print intervaloConfianza(promUP,varianzaUP,largoMuestra)

    plt.show()

def percolates(matrix,qFind):
       for i in range(0,RANGOMATRIZ):
           for k in range(0,RANGOMATRIZ):
               dato1=matrix[0][i]
               dato2=matrix[RANGOMATRIZ-1][k]
               if(dato1!=0 and dato2!=0):
                   if(encontroPercolacion(dato1,dato2,qFind)==True):
                       return True
       return False
       # for i in range(1000):
       #          #ELIJE AL AZAR VALORES QUE ESTEN EN LOS EXTREMOS DE LA MATRIZ PARA COMPROBAR CONEXION
       #       aleatoFila1=random.randrange(RANGOMATRIZ)
       #       comprobarUno=int(matrix[0][aleatoFila1])
       #       aleatoFila2=random.randrange(RANGOMATRIZ)
       #       comprobarDos=int(matrix[RANGOMATRIZ-1][aleatoFila2])
       #       if(comprobarUno !=0 and comprobarDos !=0):
       #           respuesta=qFind.connected(comprobarUno,comprobarDos)
       #           return respuesta
       #



def get_percolation_probability(p, largoMatriz):
    successes = 0.0
    trials = 0.0
    for i in xrange(CANTIDADINTENT):
        A = llenarMatrizProbabilidad(RANGOMATRIZ,p) #retorna una matriz llenada aleatoriamente con prob llenado p
        qFind=QuickFind((RANGOMATRIZ**2)+1)
        unirConexion(RANGOMATRIZ,A,qFind)
        if percolates(A,qFind):
            successes += 1
        trials += 1
    print "------------------------------------"
    print "prob llen : "+str(p)
    print "(exito , intentos) : "+str((successes,trials))
    print "probabilidad : "+str(successes/trials)

    return successes/trials


def test_percolation(largoMatriz):
    datosX = np.linspace(0, 1, CANTIDADPUNTOS)
    datosY = np.zeros(CANTIDADPUNTOS)
    for i, p in enumerate(datosX):
        if(p>0.30 and p<0.80):
         datosY[i] = get_percolation_probability(p, largoMatriz)
        elif(p<=0.30):
            datosY[i]=0.0
        elif(p>=0.80):
            datosY[i]=1.0
    graficoDispersion(datosX,datosY)

def main():
    t0 = time.time()
    test_percolation(RANGOMATRIZ)
    print "Tiempo Ejecucion : "+str(time.time()- t0)
    print "Rango Matriz: "+str(RANGOMATRIZ)
    print "Cantidad de Puntos: "+str(CANTIDADINTENT)
    print "Cantidad Puntos Grafico: "+str(CANTIDADPUNTOS)


main()

