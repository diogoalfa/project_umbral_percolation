ComputacionParalela2S-2014
==========================

Integrantes
===========

- Carlos Cordero
- Mario Ibarra
- Diego Navia
- Ricardo Lopez
- Francisco Ramirez

Intrucciones
============

Se pide ejecutar permiso para el archivo "callterminal.sh" de la forma "chmod +x callTerminal.sh"

===========================================================================
- Universidad Tecnologica Metropolitana
- Ingenieria Civil en Computación Mención Informática (21041)
- Repositorio para la asignatura de Computación Paralela, 2do Semestre 2014 


