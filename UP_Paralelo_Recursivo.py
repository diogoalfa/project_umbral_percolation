# -*- coding: utf-8 -*-
__author__ = 'macbookpro'
import time
import sys
import numpy as np
import random
import math
from StringIO import StringIO
import matplotlib.pyplot as plt
from sympy.solvers import solve
from sympy import Symbol
sys.setrecursionlimit(1000000)
starting_point=time.time()


from mpi4py import MPI

comm = MPI.COMM_WORLD  # comunicador entre dos procesadores #

rank = comm.rank     # id procesador actual #
size = comm.size     # tamaño procesador #

# Problem:
# Dada una matrix de tamaño N, cual es la probabilidad P de que la Matrix
# percole si la probabilidad de que cada punto en la que cada punto de la Matrix este
#  abierto sea p?
#
# test_percolation(N) imprime p vs P dado N.

CANTIDADPUNTOS = 100 # cantidad de puntos en el grafico entre 0 y 1
CANTIDADINTENT = 200 # number of times we try to estimate P for a given (p,N)
listaProb = np.linspace(0, 1, CANTIDADPUNTOS)
global distPuntosProb
distPuntosProb = CANTIDADPUNTOS/size
RANGOMATRIZ = 1000

class QuickFind:
    def __init__(self, n):
        global id
        id = np.arange(0, n, 1)
        #print "rango id : " + str(len(id))

    def connected(self, id1, id2):
        if (id[id1] == id[id2]):
            return True
        else:
            return False

    def union(self, id1, id2):
        id_1 = id[id1]
        id_2 = id[id2]
        for i in range(len(id)):
            if (id[i] == id_1):
                id[i] = id_2

class ManejoFichero:
    def __init__(self, nombreArchivo, accion):
        self.nombreArchivo = nombreArchivo
        self.accion = accion


    def grabarMatriz(self, matriz):
        if (self.accion == "w"):
            self.archivo = open(self.nombreArchivo, self.accion)
            for i in range(len(matriz)):
                for j in range(len(matriz[0])):
                    self.archivo.write(str(int(matriz[i][j])))
                    self.archivo.write(" ")
                self.archivo.write("\n")
            self.archivo.close()
            return "1"
        return 0

    def leerFichero(self):
        if (self.accion == "r"):
            self.archivo = open(self.nombreArchivo, self.accion)
            matriz = self.archivo.read()
            self.archivo.close()
            matriz = np.genfromtxt(StringIO(matriz))
            return matriz
        return 0

    def guardarDato(self, valorX, valorY):
        if (self.accion == "a"):
            self.archivo = open(self.nombreArchivo, self.accion)
            self.archivo.write(str(valorX) + " " + str(valorY) + "\n")
            self.archivo.close()

    def guardarDatoUnico(self, valorX):
        if (self.accion == "a"):
            self.archivo = open(self.nombreArchivo, self.accion)
            self.archivo.write(str(valorX)+"\n")
            self.archivo.close()

    def setAccion(self,accion):
        self.accion=accion
    def setNombreArchivo(self,nombreArchivo):
        self.nombreArchivo=nombreArchivo

def umbral_de_percolacion(data):
    for i, y  in enumerate(data[:,1]):
        if(y >= 0.5):
            umbral = (data[i,0]+data[i-1,0])/2
            return umbral

def random_matrix(p,N):
    # p is probability of (i,j) being open
    return np.random.random((N,N)) < p

def fill(grid, filled, i, j):
    N = len(grid)
    if j >= 0 and j < N and i > 0 and i < N and grid[i][j] and not filled[i][j]:
        # fill only if inside grid // llenar sólo si la rejilla en el interior
        # fill only if grid is open at (i,j) | llenar sólo si la rejilla está abierta en
        # don't fill anything already filled|no llene nada ya está lleno
        if filled[i-1][j]: # top
            filled[i][j] = True
        elif j > 0 and filled[i][j-1]: # left
            filled[i][j] = True
        elif j < N-1 and filled[i][j+1]: # right
            filled[i][j] = True

        # if current filled, keep filling recursively
        if filled[i][j]:
            fill(grid, filled, i+1, j) # down
            fill(grid, filled, i, j-1) # left
            fill(grid, filled, i, j+1)

def percolates(grid):
    N = len(grid)
    filled = np.zeros((N,N))

    filled[0] = grid[0] # first row is the same// primera fila es la misma
    for j in xrange(N):
        fill(grid, filled, 1, j)
    # print "filled largo : "+str(N)
    # print filled
    # print "np.Any : "+str(np.any(filled[N-1]))
    return np.any(filled[N-1]) # if any filled in the last row, it percolates
                                #Si cualquier llenado en la última fila, se filtra

def get_percolation_probability(p,N):
    successes = 0.0
    trials = 0.0
    for i in xrange(CANTIDADINTENT):
        A = random_matrix(p,N) #retorna una matriz llenada aleatoriamente con prob llenado p
        # A = llenarMatrizIdentiProb(A)
        if percolates(A):
            successes += 1
        trials += 1
    #print A
    return successes/trials

def llenarMatrizIdentiProb(matrix):
    rangoMatriz=len(matrix)
    iden=1
    for i in range(rangoMatriz):
        for j in range(rangoMatriz):
            if(matrix[i][j]==1):
                 matrix[i][j]=iden
                 iden+=1
    return matrix

def test_percolation(N):

    P = np.linspace(0,1,CANTIDADPUNTOS)
    Y = np.zeros(CANTIDADPUNTOS)
    for i,p in enumerate(P):
        Y[i] = get_percolation_probability(p,N)
    #print ", ".join(map(str,P))
    #datos=[]
    #datos.append(P)
    #datos.append(Y)
    #X= umbral_de_percolacion(datos)
    #for i in range(len(P)):
	#datos.append((P[i],Y[i]))
    #X= umbral_de_percolacion(datos)
    #print datos
    data =np.array([P,Y])
    data = data.T
    U =umbral_de_percolacion(data)
    print "El Umbral de Percolacion es: ",U
    plt.plot(P,Y,"*-")
    plt.plot([U],[0.5],"o")
    plt.show()

def intervaloConfianza(promUP,varianzaUP,largoMuestra):
    interX=promUP-(1.95*math.sqrt(varianzaUP)/math.sqrt(largoMuestra))
    interY=promUP+(1.95*math.sqrt(varianzaUP)/math.sqrt(largoMuestra))
    return interX,interY

def promPinflex(umbPer):
    mf=ManejoFichero("datosPercolacion.txt","a")
    mf.guardarDatoUnico(umbPer)
    mf.setAccion("r")
    datos=mf.leerFichero()
    #print datos
    #print "largo array datos : "+str(len(datos))+" | largo "+str(len(datos[0]))
    largo=len(datos)
    suma1=0
    for i in range(largo):
        if(datos[i]<0.9 and datos[i]<0.9):
            suma1=suma1+datos[i]
    promUP=suma1/largo
    suma3=0
    for i in range(largo):
        if datos[i]<=1 and datos[i]>=0:
            suma3=suma3+(datos[i]-promUP)**2
            #print suma3
    #print suma3,",",largo
    varianza=suma3/largo
    #print "varianza ", varianza
    return promUP,varianza,largo

def alternativeUmbral(poli):
    # print poli
    umbral_ini=poli.deriv()
    umbral_end=umbral_ini.deriv()
    # print umbral_ini
    # print umbral_end
    # print umbral_end[0]
    # print umbral_end[1]
    umbral=(umbral_end[0]*-1)/umbral_end[1]+0.4
    return umbral

def graficoDispersion(llenado,probabilidad):
    # X = llenado     Y= probabilidad
    plt.title("Probabilidad Percolacion vs Apertura")
    plt.plot(llenado,probabilidad)
    linTend =np.polyfit(llenado,probabilidad,3)
    polinomio =np.poly1d(linTend)
    # umbral=alternativeUmbral(polinomio);
    data=np.array([llenado,probabilidad])
    data=data.T
    uP=umbral_de_percolacion(data)
    # uP=umbral
    plt.plot(uP,0.5,"o")
    print "----------------------------------------"
    #print "Umbral Percolacion :"+str(uP)
    print 'Cantidad Puntos : ',CANTIDADPUNTOS
    print 'CANTIDAD INTENTOS : ',CANTIDADINTENT
    print 'Rango Matriz : ',RANGOMATRIZ
    print 'Cantidad Procesadores : ',size
    print "----------------------------------------"
    promUP,varianzaUP,largoMuestra=promPinflex(uP)
    #print "Promedio UP : "+str(promUP)
    print "Umbral Percolacion :"+str(promUP)
    print "Varianza UP : "+str(varianzaUP)
    print "intervalos de confianza : ", intervaloConfianza(promUP,varianzaUP,largoMuestra)
    plt.show()

#---------------------------MAIN----------------------------------------------------
if rank != 0:
    if rank == 1:
        contador=-1
        ini=0   #rango de inicio
        fin=0   #rango final
        numProcesador=0
        for i in range(len(listaProb)):
            contador+=1
            if(contador==0):
                ini=fin
                #print 'ini : ',ini
            elif contador == distPuntosProb:
                contador = -1
                fin = listaProb[i]
                numProcesador += 1
                rang = (ini, fin)
                comm.send(rang, numProcesador)
    #print 'rango de trabajo : '

    ini, fin=comm.recv(source=1)
    datosX = np.linspace(ini, fin, distPuntosProb)
    datosY = np.zeros(distPuntosProb)
    for i, p in enumerate(datosX):
        datosY[i] = get_percolation_probability(p, RANGOMATRIZ)

    comm.send(datosX, dest=1)
    comm.send(datosY, dest=1)

    print 'Procesador :', rank
    if rank==1:
        dataX=[]
        dataY=[]
        for i in range(1, size):
           dataX = np.append(dataX, comm.recv(source=i))
           dataY = np.append(dataY, comm.recv(source=i))
        elapsed_time = time.time()-starting_point

        print "----------------------------------------"
        print "sec: ", elapsed_time
        elapsed_time_int = int(elapsed_time)
        elapsed_time_minutes = elapsed_time_int/60
        elapsed_time_seconds = elapsed_time_int%60
        print "Time: " + str(elapsed_time_minutes) + ":" + str(elapsed_time_seconds)
        graficoDispersion(dataX, dataY)



#---------------------------------------------------------------------------------------------------------
