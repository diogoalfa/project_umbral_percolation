__author__ = 'macbookpro'
# coding=utf-8
import numpy as np
import matplotlib.pyplot as plt
import time

# Problem:
# Dada una matrix de tamaño N, cual es la probabilidad P de que la Matrix
# percole si la probabilidad de que cada punto en la que cada punto de la Matrix este
#  abierto sea p?
#
# test_percolation(N) imprime p vs P dado N.

RESOLUTION = 100 # cantidad de puntos en el grafico entre 0 y 1
SAMPLE_SIZE = 100 # number of times we try to estimate P for a given (p,N)


def umbral_de_percolacion(data):
    for i, y  in enumerate(data[:,1]):
        if(y > 0.5):
            umbral = (data[i,0]+data[i-1,0])/2

            return umbral

def random_matrix(p,N):
    # p is probability of (i,j) being open
    return np.random.random((N,N)) < p


def fill(grid, filled, i, j):
    N = len(grid)
    if j >= 0 and j < N and i > 0 and i < N and grid[i][j] and not filled[i][j]:
        # fill only if inside grid // llenar sólo si la rejilla en el interior
        # fill only if grid is open at (i,j) | llenar sólo si la rejilla está abierta en
        # don't fill anything already filled|no llene nada ya está lleno
        if filled[i-1][j]: # top
            filled[i][j] = True
        elif j > 0 and filled[i][j-1]: # left
            filled[i][j] = True
        elif j < N-1 and filled[i][j+1]: # right
            filled[i][j] = True

        # if current filled, keep filling recursively
        if filled[i][j]:
            fill(grid, filled, i+1, j) # down
            fill(grid, filled, i, j-1) # left
            fill(grid, filled, i, j+1)


def percolates(grid):
    N = len(grid)
    filled = np.zeros((N,N))

    filled[0] = grid[0] # first row is the same// primera fila es la misma
    for j in xrange(N):
        fill(grid, filled, 1, j)
    # print "filled largo : "+str(N)
    # print filled
    # print "np.Any : "+str(np.any(filled[N-1]))
    return np.any(filled[N-1]) # if any filled in the last row, it percolates
                                #Si cualquier llenado en la última fila, se filtra



def get_percolation_probability(p,N):
    successes = 0.0
    trials = 0.0
    for i in xrange(SAMPLE_SIZE):
        A = random_matrix(p,N) #retorna una matriz llenada aleatoriamente con prob llenado p
        # A = llenarMatrizIdentiProb(A)
        if percolates(A):
            successes += 1
        trials += 1
    #print A
    return successes/trials

def llenarMatrizIdentiProb(matrix):
    rangoMatriz=len(matrix)
    iden=1
    for i in range(rangoMatriz):
        for j in range(rangoMatriz):
            if(matrix[i][j]==1):
                 matrix[i][j]=iden
                 iden+=1
    return matrix


def test_percolation():
    N=int(input("INgrese Rango Matriz Cuadrada: "))
    RESOLUTION=int(input("Ingrese Cantidad Puntos del Grafico: "))
    SAMPLE_SIZE=int(input("Ingrese Cantidad de Simulaciones: "))


    P = np.linspace(0,1,RESOLUTION)
    Y = np.zeros(RESOLUTION)
    for i,p in enumerate(P):
        Y[i] = get_percolation_probability(p,N)


    #print ", ".join(map(str,P))
    #datos=[]
    #datos.append(P)
    #datos.append(Y)
    #X= umbral_de_percolacion(datos)
    #for i in range(len(P)):
	#datos.append((P[i],Y[i]))
    #X= umbral_de_percolacion(datos)
    #print datos
    data =np.array([P,Y])
    data = data.T
    U =umbral_de_percolacion(data)
    print "El Umbral de Percolacion es: ",U
    plt.plot(P,Y,"*-")
    plt.plot([U],[0.5],"o")
    print "Cantidad de Intentos : ",SAMPLE_SIZE
    print "Cantidad de Puntos : ",RESOLUTION
    plt.show()


if __name__ == '__main__':


	t0=time.time()
	test_percolation()
	print time.time() -t0
