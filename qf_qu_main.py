import random
from StringIO import StringIO
import matplotlib.pyplot as plt
import numpy as np
import sympy as sy


__author__ = 'FisAnalysis Technology'



class ManejoFichero:
    def __init__(self,nombreArchivo,accion):
        self.nombreArchivo=nombreArchivo
        self.accion=accion


    def grabarMatriz(self,matriz):
        if(self.accion=="w"):
            self.archivo=open(self.nombreArchivo,self.accion)
            for i in range(len(matriz)):
                for j in range(len(matriz[0])):
                    self.archivo.write(str(int(matriz[i][j])))
                    self.archivo.write(" ")
                self.archivo.write("\n")
            self.archivo.close()
            return "1"
        return 0

    def leerFichero(self):
        if(self.accion=="r"):
            self.archivo=open(self.nombreArchivo,self.accion)
            matriz=self.archivo.read()
            self.archivo.close()
            matriz=np.genfromtxt(StringIO(matriz))
            return matriz
        return 0
    def guardarDato(self,valorX,valorY):
        if(self.accion=="a"):
            self.archivo=open(self.nombreArchivo,self.accion)
            self.archivo.write(str(valorX)+" "+str(valorY)+"\n")
            self.archivo.close()


# QuickFind = es una estructura de datos para guardar que numeros estan conectados y despues corroborar si esta conectados en tiempo nlog(n)

class QuickFind:

    def __init__(self,n):
       global id
       id=np.arange(0,n,1)
       print "rango id : "+str(len(id))

    def connected(self,id1,id2):
        if(id[id1]==id[id2]):
            return True
        else:
            return False

    def union(self,id1,id2):
        id_1=id[id1]
        id_2=id[id2]
        for i in range(len(id)):
            if(id[i]==id_1):
                id[i]=id_2

def contarPuntosMatriz(matrix,rangoMatriz):
    contador=0
    for i in range(rangoMatriz):
        for j in range(rangoMatriz):
            if(matrix[i][j]==1):
                contador+=1
    return contador
def llenarMatrizProbabilidad(rangoMatriz,probabilidad):
    matriz=np.zeros((rangoMatriz,rangoMatriz))
    cantPuntos=rangoMatriz*rangoMatriz
    print "probabilidad de llenado +: "+str(probabilidad)
    cantLlenarPuntos=int(probabilidad*cantPuntos)
    print "cantidad puntos por LLenar :"+str(cantLlenarPuntos)
    i=1
    while(i<=cantLlenarPuntos):
        aleatorioX=random.randrange(rangoMatriz)
        aleatorioY=random.randrange(rangoMatriz)
        if(matriz[aleatorioX][aleatorioY]==0):
            matriz[aleatorioX][aleatorioY]=i
            i+=1
    return matriz

def llenarMatrizProb(rangoMatriz,probabilidad):
    matrix=np.random.binomial(1,probabilidad,size=(rangoMatriz,rangoMatriz))
    iden=1
    for i in range(rangoMatriz):
        for j in range(rangoMatriz):
            if(matrix[i][j]==1):
                 matrix[i][j]=iden
                 iden+=1
    return matrix

def umbral_de_percolacion(data):
    for i, y  in enumerate(data[:,1]):
        if(y > 0.5):
            umbral = (data[i,0]+data[i-1,0])/2
            return umbral


# funcion que va uniendo los puntos que estan conectados dado un rango solamente.
def unirConexion(rangoMatriz,matrix,q_find):
    for j in range(0,rangoMatriz):
        for i in range(rangoMatriz-1):
            if(matrix[i][j]!=0 and matrix[i+1][j]!= 0):
                #print "matriz :"+str(matrix[i][j])+"| matriz :"+str(matrix[i+1][j])
                q_find.union(int(matrix[i][j]),int(matrix[i+1][j]))
            if(matrix[j][i]!=0 and matrix[j][i+1]!=0):
                q_find.union(int(matrix[j][i]),int(matrix[j][i+1]))
                #print (int(matrix[j][i]),int(matrix[j][i+1]))
        #print "Iter V: "+str(j)

def graficoDispersion(llenado,probabilidad):
    # X = llenado     Y= probabilidad
    plt.title("Probabilidad Percolacion vs Apertura")
    llen=[]
    plt.axis([0,1,0,1])
    for i in range(len(llenado)):
        llen.append(llenado[i])
    probabilidad.sort()
    rg=cantPrueba*0.2
    mp=0.8
    # for i in range(int(rg)):
    #     mp=mp+((i/(rg*10.0)))
    #     llen.append(mp)
    #     probabilidad.append(1.0)

    plt.plot(llen,probabilidad,'o-')
    linTend =np.polyfit(llen,probabilidad,5)
    polinomio =np.poly1d(linTend)
    plt.plot(llen,polinomio(llen),"r--")
    #-------Analisis de datos --------------
    # soluciones=analisisDatos(polinomio)
    # puntoInflexion=soluciones[1]
    # #puntoInflexion=soluciones[0]
    # print puntoInflexion
    # pY=str(puntoInflexion)
    # pI=""+pY[0]
    # pI=pI+pY[1]
    # pI=pI+pY[2]
    # pI=pI+pY[3]
    # pI=pI+pY[4]
    # piX=float(pI)
    # piY=polinomio(piX)
    # print "Punto Inflexion :"
    # print (piX,piY)
    # #--------------------------------------------------
    #Saca el promedio y la varianza
    # promPinf()
    # if(piX<=0.9 and piX<=0.9):
    #     mf=ManejoFichero("datosPercolacion.txt","a")
    #     mf.guardarDato(piX,piY)
    #     plt.plot(piX,piY,"o")



    #data.append([llen],[probabilidad])
    #data.append(probabilidad)


    data=np.array([llen,probabilidad])
    data=data.T
    print umbral_de_percolacion(data)


    plt.show()


def analisisDatos(polinomio):
    func=polinomio
    deriv1=func.deriv()
    deriv2=deriv1.deriv()
    x=sy.symbols('x')
    #--Elevado a 5 ---
    d=deriv2[0]
    c=deriv2[1]
    b=deriv2[2]
    a=deriv2[3]
    sols=sy.solve(a*x**3 + b*x**2 + c*x + d,x)
    #-----Elevado a 3
    # a=deriv2[1]
    # b=deriv2[0]
    # sols=sy.solve(a*x + b)

    return sols

def promPinf():
    mf=ManejoFichero("datosPercolacion.txt","r")
    datos=mf.leerFichero()
    #print "largo array datos : "+str(len(datos))+" | largo "+str(len(datos[0]))
    largo=len(datos)
    suma1=0
    suma2=0
    for i in range(largo):
       if(datos[i][0]<0.9 and datos[i][1]<0.9):
           suma1=suma1+datos[i][0]
           suma2=suma2+datos[i][1]
    promX=suma1/largo
    promY=suma2/largo
    suma3=0
    for i in range(largo):
        suma3=suma3+(datos[i][0]-promX)**2
    varianza=suma3/largo
    print "PROMEDIO P.I. |x : "+str(promX)+"|y : "+str(promY)
    print "Varianza x : "+str(varianza)

def encontroPercolacion(dato1,dato2,q_find):
        if(q_find.connected(dato1,dato2)==True):
            return 1
        else:
            return 0


def main():
    prob=[]
    llen=[]
    global cantPrueba
    cantPrueba=500
    llenadoProb=np.linspace(0,1,cantPrueba)
    for j in range(len(llenadoProb)):
        print "---------------------------------------------------------------"
        rangoMatriz=10
        matrix=llenarMatrizProbabilidad(rangoMatriz,llenadoProb[j])
        q_find=QuickFind(int(rangoMatriz**2)+1)
        unirConexion(rangoMatriz,matrix,q_find)
        if(llenadoProb[j]>0.52 and llenadoProb[j]<0.54):
            mf=ManejoFichero("mapa.txt","w")
            mf.grabarMatriz(matrix)
        contadorConexion=0
        contadorIntentos=1
        # for i in range(rangoMatriz):
        #    for k in range(rangoMatriz):
        #        comprobarUno=int(matrix[0][i])
        #        comprobarDos=int(matrix[rangoMatriz-1][k])
        #        if(comprobarUno!=0 and comprobarDos!=0 ):
        #            respuesta=q_find.connected(comprobarUno,comprobarDos)
        #
        #            if(respuesta==True):
        #               if(llenadoProb[j]==50):
        #                 print "comprobar 1= "+str(comprobarUno)+"|comprobar 2 = "+str(comprobarDos)
        #               contadorConexion+=1
        #               break
        #    contadorIntentos+=1
        for i in range(0,rangoMatriz):
           for k in range(0,rangoMatriz):
               dato1=matrix[0][i]
               dato2=matrix[rangoMatriz-1][k]
               if(dato1!=0 and dato2!=0):
                   enconPer=encontroPercolacion(dato1,dato2,q_find)
                   contadorConexion=contadorConexion+enconPer
                   contadorIntentos+=1
                   if(enconPer==1):
                       break




        # respuesta=False
        # for i in range(1000):
        #         #ELIJE AL AZAR VALORES QUE ESTEN EN LOS EXTREMOS DE LA MATRIZ PARA COMPROBAR CONEXION
        #      aleatoFila1=random.randrange(rangoMatriz)
        #      comprobarUno=int(matrix[0][aleatoFila1])
        #      aleatoFila2=random.randrange(rangoMatriz)
        #      comprobarDos=int(matrix[rangoMatriz-1][aleatoFila2])
        #      if(comprobarUno !=0 and comprobarDos !=0):
        #          respuesta=q_find.connected(comprobarUno,comprobarDos)
        #      if(respuesta==True):
        #          contadorConexion+=1
        #      contadorIntentos+=1

        prob.append(contadorConexion/float(contadorIntentos))
        print "Cantidad Conexiones exitosas : "+str(contadorConexion)
        print "Cantidad Intentos : "+str(contadorIntentos)
        print "Probabilidad de exito : "+str(contadorConexion/float(contadorIntentos))
        print "---------------------------------------------------------------"
        #llenadoProb[j]=llenadoProb[j]-0.15


    graficoDispersion(llenadoProb,prob)



main()








