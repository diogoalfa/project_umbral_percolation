
__author__ = 'macbookpro'

'''
Created on 20/08/2014

@author: macbookpro
'''


import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

def llenarProbabilidad(rangoMatriz,probabilidad):
    matrix=np.random.binomial(1,probabilidad,size=(rangoMatriz,rangoMatriz))
    return matrix

fig, ax = plt.subplots()
line, = ax.plot(np.random.rand(10))
ax.set_ylim(0, 1)

def update(data):
    line.set_ydata(data)
    return line


def data_gen():
    while True: yield np.random.rand(10)

ani = animation.FuncAnimation(fig, update, data_gen, interval=100)


plt.show()